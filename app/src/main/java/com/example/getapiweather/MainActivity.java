package com.example.getapiweather;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private DataAdapter adapter;
    private Service service;
    private List<WeatherObservation> list;

    private static Retrofit retrofit;
    private static String BASE_URL = "http://api.geonames.org/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupView();
        networkRequest();
    }

    private void setupView() {
        recyclerView = findViewById(R.id.rv);
    }

    private void networkRequest() {
        service = getRetrofitInstance().create(Service.class);
        service.getData("http://api.geonames.org/weatherJSON?north=44.1&south=-9.9&east=-22.4&west=55.2&username=demos")
                .enqueue(new Callback<ModelWeather>() {
                    @Override
                    public void onResponse(Call<ModelWeather> call, Response<ModelWeather> response) {
                        ModelWeather weather = response.body();
                        list = weather.getWeatherObservations();
                        showData(list);
                    }

                    @Override
                    public void onFailure(Call<ModelWeather> call, Throwable t) {

                    }
                });
    }

    private void showData(List<WeatherObservation> list) {
        recyclerView.setLayoutManager(
                new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        adapter = new DataAdapter(this, list);
        recyclerView.setAdapter(adapter);
    }

    public static Retrofit getRetrofitInstance(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        if (retrofit == null){
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }
        return retrofit;
    }
}

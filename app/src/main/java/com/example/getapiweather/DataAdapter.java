package com.example.getapiweather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private Context context;
    private List<WeatherObservation> list;

    public DataAdapter(Context context, List<WeatherObservation> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_data, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        WeatherObservation data = list.get(position);
        holder.tvLng.setText(data.getLng().toString());
        holder.tvObservation.setText(data.getObservation());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvLng, tvObservation;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvLng = itemView.findViewById(R.id.card_lng);
            tvObservation = itemView.findViewById(R.id.card_observation);
        }
    }
}

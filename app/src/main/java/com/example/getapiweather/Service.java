package com.example.getapiweather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface Service {
    @GET
    Call<ModelWeather> getData(@Url String url);
}
